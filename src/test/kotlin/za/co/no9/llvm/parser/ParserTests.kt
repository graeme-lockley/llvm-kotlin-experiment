package za.co.no9.llvm.parser

import io.kotlintest.shouldBe
import io.kotlintest.specs.ExpectSpec
import za.co.no9.llvm.ast.typelessAST.*
import za.co.no9.llvm.data.either.Right
import za.co.no9.llvm.data.lexer.LocationCoordinate
import za.co.no9.llvm.data.lexer.LocationRange
import za.co.no9.llvm.data.parser.Ok
import za.co.no9.llvm.phases.parsing.*
import za.co.no9.llvm.type.BooleanType
import za.co.no9.llvm.type.FloatType
import za.co.no9.llvm.type.IntType
import za.co.no9.llvm.type.StringType


class ModuleTests : ExpectSpec({
    context("argument") {
        expect("x: Int") {
            val reply =
                    (argument(lexer("x : Int")) as Ok).reply

            reply.name shouldBe "x"
            reply.referencedType.type shouldBe IntType
        }
    }


    context("declaration") {
        expect("sum (a: Int, b: Int):Int = a + b") {
            val reply =
                    (declaration(lexer("sum (a: Int, b: Int):Int = a + b")) as Ok).reply as FunctionDeclaration

            reply.arguments.map { it.name } shouldBe listOf("a", "b")
            reply.arguments.map { it.referencedType.type } shouldBe listOf(IntType, IntType)
            reply.name shouldBe "sum"
            reply.returnReferencedType.type shouldBe IntType
            reply.location shouldBe LocationRange(LocationCoordinate(0, 1, 1), LocationCoordinate(31, 1, 32))
        }

        expect("zero (): Int = 0") {
            val reply =
                    (declaration(lexer("zero (): Int = 0")) as Ok).reply as FunctionDeclaration

            reply.arguments shouldBe listOf()
            reply.name shouldBe "zero"
            reply.location shouldBe LocationRange(LocationCoordinate(0, 1, 1), LocationCoordinate(15, 1, 16))
        }

        expect("zero: Int = 0") {
            val reply =
                    (declaration(lexer("zero: Int = 0")) as Ok).reply as ValueDeclaration

            reply.name shouldBe "zero"
            reply.location shouldBe LocationRange(LocationCoordinate(0, 1, 1), LocationCoordinate(12, 1, 13))
        }
    }


    context("module") {
        expect("sum (a: Int, b:Int):Int = a + b\ninc (b: Int):Int = b + 1") {
            val text =
                    "sum (a: Int, b: Int): Int = a + b\n" +
                            "inc (b: Int): Int = b + 1"

            val reply =
                    (parse(lexer(text)) as Right).right

            reply.declarations.size shouldBe 2

            reply.declarations[0].name shouldBe "sum"
            reply.declarations[1].name shouldBe "inc"
        }
    }
})


class ExpressionTests : ExpectSpec({
    context("factor") {
        expect("True") {
            val reply =
                    (factor(lexer("True")) as Ok).reply

            (reply is ConstantBoolean) shouldBe true
            (reply as ConstantBoolean).value shouldBe true
        }


        expect("False") {
            val reply =
                    (factor(lexer("False")) as Ok).reply

            (reply is ConstantBoolean) shouldBe true
            (reply as ConstantBoolean).value shouldBe false
        }


        expect("123.45") {
            val reply =
                    (factor(lexer("123.45")) as Ok).reply

            (reply is ConstantFloat) shouldBe true
            (reply as ConstantFloat).value shouldBe 123.45f
        }


        expect("123") {
            val reply =
                    (factor(lexer("123")) as Ok).reply

            (reply is ConstantInt) shouldBe true
            (reply as ConstantInt).value shouldBe 123
        }


        expect("\"hello world\"") {
            val reply =
                    (factor(lexer("\"hello world\"")) as Ok).reply

            (reply is ConstantString) shouldBe true
            (reply as ConstantString).value shouldBe "hello world"
        }


        expect("(123)") {
            val reply =
                    (factor(lexer("(123)")) as Ok).reply

            (reply is NestedExpression) shouldBe true
            ((reply as NestedExpression).expression is ConstantInt) shouldBe true
        }
    }


    context("unaryExpressionNotPlusMinus") {
        expect("True") {
            val reply =
                    (unaryExpressionNotPlusMinus(lexer("True")) as Ok).reply

            (reply is ConstantBoolean) shouldBe true
            (reply as ConstantBoolean).value shouldBe true
        }

        expect(" !True") {
            val reply =
                    (unaryExpressionNotPlusMinus(lexer("!True")) as Ok).reply

            (reply is UnaryOperation) shouldBe true
            (reply as UnaryOperation).operator shouldBe "!"
            (reply.expression as ConstantBoolean).value shouldBe true
        }
    }


    context("unaryExpression") {
        expect("10") {
            val reply =
                    (unaryExpression(lexer("10")) as Ok).reply

            (reply is ConstantInt) shouldBe true
            (reply as ConstantInt).value shouldBe 10
        }

        expect("+10") {
            val reply =
                    (unaryExpression(lexer("+10")) as Ok).reply

            (reply is UnaryOperation) shouldBe true
            (reply as UnaryOperation).operator shouldBe "+"
            (reply.expression as ConstantInt).value shouldBe 10
        }

        expect("-10") {
            val reply =
                    (unaryExpression(lexer("-10")) as Ok).reply

            (reply is UnaryOperation) shouldBe true
            (reply as UnaryOperation).operator shouldBe "-"
            (reply.expression as ConstantInt).value shouldBe 10
        }
    }


    context("multiplicativeExpression") {
        expect("10") {
            val reply =
                    (multiplicativeExpression(lexer("10")) as Ok).reply

            (reply is ConstantInt) shouldBe true
            (reply as ConstantInt).value shouldBe 10
        }

        expect("10 * 11") {
            val reply =
                    (multiplicativeExpression(lexer("10 * 11")) as Ok).reply

            (reply is BinaryOperation) shouldBe true
            (reply as BinaryOperation).operator shouldBe "*"
            (reply.left as ConstantInt).value shouldBe 10
            (reply.right as ConstantInt).value shouldBe 11
        }

        expect("10 / 11") {
            val reply =
                    (multiplicativeExpression(lexer("10 / 11")) as Ok).reply

            (reply is BinaryOperation) shouldBe true
            (reply as BinaryOperation).operator shouldBe "/"
            (reply.left as ConstantInt).value shouldBe 10
            (reply.right as ConstantInt).value shouldBe 11
        }

        expect("10 % 11") {
            val reply =
                    (multiplicativeExpression(lexer("10 % 11")) as Ok).reply

            (reply is BinaryOperation) shouldBe true
            (reply as BinaryOperation).operator shouldBe "%"
            (reply.left as ConstantInt).value shouldBe 10
            (reply.right as ConstantInt).value shouldBe 11
        }
    }


    context("additiveExpression") {
        expect("10") {
            val reply =
                    (additiveExpression(lexer("10")) as Ok).reply

            (reply is ConstantInt) shouldBe true
            (reply as ConstantInt).value shouldBe 10
        }

        expect("10 + 11") {
            val reply =
                    (additiveExpression(lexer("10 + 11")) as Ok).reply

            (reply is BinaryOperation) shouldBe true
            (reply as BinaryOperation).operator shouldBe "+"
            (reply.left as ConstantInt).value shouldBe 10
            (reply.right as ConstantInt).value shouldBe 11
        }

        expect("10 - 11") {
            val reply =
                    (additiveExpression(lexer("10 - 11")) as Ok).reply

            (reply is BinaryOperation) shouldBe true
            (reply as BinaryOperation).operator shouldBe "-"
            (reply.left as ConstantInt).value shouldBe 10
            (reply.right as ConstantInt).value shouldBe 11
        }
    }


    context("relationalExpression") {
        expect("True") {
            val reply =
                    (relationalExpression(lexer("True")) as Ok).reply

            (reply is ConstantBoolean) shouldBe true
            (reply as ConstantBoolean).value shouldBe true
        }

        expect("True < False") {
            val reply =
                    (relationalExpression(lexer("True < False")) as Ok).reply

            (reply is BinaryOperation) shouldBe true
            (reply as BinaryOperation).operator shouldBe "<"
            (reply.left as ConstantBoolean).value shouldBe true
            (reply.right as ConstantBoolean).value shouldBe false
        }

        expect("True <= False") {
            val reply =
                    (relationalExpression(lexer("True <= False")) as Ok).reply

            (reply is BinaryOperation) shouldBe true
            (reply as BinaryOperation).operator shouldBe "<="
            (reply.left as ConstantBoolean).value shouldBe true
            (reply.right as ConstantBoolean).value shouldBe false
        }

        expect("True > False") {
            val reply =
                    (relationalExpression(lexer("True > False")) as Ok).reply

            (reply is BinaryOperation) shouldBe true
            (reply as BinaryOperation).operator shouldBe ">"
            (reply.left as ConstantBoolean).value shouldBe true
            (reply.right as ConstantBoolean).value shouldBe false
        }

        expect("True >= False") {
            val reply =
                    (relationalExpression(lexer("True >= False")) as Ok).reply

            (reply is BinaryOperation) shouldBe true
            (reply as BinaryOperation).operator shouldBe ">="
            (reply.left as ConstantBoolean).value shouldBe true
            (reply.right as ConstantBoolean).value shouldBe false
        }
    }


    context("equalityExpression") {
        expect("10") {
            val reply =
                    (equalityExpression(lexer("10")) as Ok).reply

            (reply is ConstantInt) shouldBe true
            (reply as ConstantInt).value shouldBe 10
        }

        expect("10 == 11") {
            val reply =
                    (equalityExpression(lexer("10 == 11")) as Ok).reply

            (reply is BinaryOperation) shouldBe true
            (reply as BinaryOperation).operator shouldBe "=="
            (reply.left as ConstantInt).value shouldBe 10
            (reply.right as ConstantInt).value shouldBe 11
        }

        expect("10 != 11") {
            val reply =
                    (equalityExpression(lexer("10 != 11")) as Ok).reply

            (reply is BinaryOperation) shouldBe true
            (reply as BinaryOperation).operator shouldBe "!="
            (reply.left as ConstantInt).value shouldBe 10
            (reply.right as ConstantInt).value shouldBe 11
        }
    }


    context("conditionalAndExpression") {
        expect("10") {
            val reply =
                    (conditionalAndExpression(lexer("10")) as Ok).reply

            (reply is ConstantInt) shouldBe true
            (reply as ConstantInt).value shouldBe 10
        }

        expect("True && False") {
            val reply =
                    (conditionalAndExpression(lexer("True && False")) as Ok).reply

            (reply is BinaryOperation) shouldBe true
            (reply as BinaryOperation).operator shouldBe "&&"
            (reply.left as ConstantBoolean).value shouldBe true
            (reply.right as ConstantBoolean).value shouldBe false
        }
    }


    context("conditionalOrExpression") {
        expect("10") {
            val reply =
                    (conditionalOrExpression(lexer("10")) as Ok).reply

            (reply is ConstantInt) shouldBe true
            (reply as ConstantInt).value shouldBe 10
        }

        expect("True || False") {
            val reply =
                    (conditionalOrExpression(lexer("True || False")) as Ok).reply

            (reply is BinaryOperation) shouldBe true
            (reply as BinaryOperation).operator shouldBe "||"
            (reply.left as ConstantBoolean).value shouldBe true
            (reply.right as ConstantBoolean).value shouldBe false
        }
    }
})


class TypeTests : ExpectSpec({
    context("type") {
        expect("Int") {
            val reply =
                    (type(lexer("Int")) as Ok).reply

            reply.type shouldBe IntType
        }

        expect("Boolean") {
            val reply =
                    (type(lexer("Boolean")) as Ok).reply

            reply.type shouldBe BooleanType
        }

        expect("String") {
            val reply =
                    (type(lexer("String")) as Ok).reply

            reply.type shouldBe StringType
        }

        expect("Float") {
            val reply =
                    (type(lexer("Float")) as Ok).reply

            reply.type shouldBe FloatType
        }
    }
})
