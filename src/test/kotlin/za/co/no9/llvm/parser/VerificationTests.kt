package za.co.no9.llvm.parser

import io.kotlintest.shouldBe
import io.kotlintest.specs.ExpectSpec
import za.co.no9.llvm.ast.typelessAST.Module
import za.co.no9.llvm.data.either.Either
import za.co.no9.llvm.data.either.Left
import za.co.no9.llvm.data.lexer.LocationCoordinate
import za.co.no9.llvm.data.lexer.LocationRange
import za.co.no9.llvm.phases.DuplicateDeclaration
import za.co.no9.llvm.phases.Error
import za.co.no9.llvm.phases.UnknownNameReference
import za.co.no9.llvm.phases.parsing.lexer
import za.co.no9.llvm.phases.typing.verify


class VerificationTests : ExpectSpec({
    context("Unknown Name Reference") {
        expect("sum (a: Int, b: Int): Int = a + xx") {
            val verifiedAST =
                    parse("sum (a: Int, b: Int): Int = a + xx")
                            .andThen { verify(it) } as Left

            verifiedAST.left.size shouldBe 1
            verifiedAST.left[0] shouldBe UnknownNameReference("xx", LocationRange(LocationCoordinate(32, 1, 33), LocationCoordinate(33, 1, 34)))
        }
    }

    context("Duplicate Declaration") {
        expect("sum (a: Int, b: Int): Int = a + b\n" +
                "sum (a: Int, b: Int): Int = a + b") {

            val verifiedAST =
                    parse("sum (a: Int, b: Int): Int = a + b\nsum (a: Int, b: Int): Int = a + b")
                            .andThen { verify(it) } as Left

            verifiedAST.left.size shouldBe 1
            verifiedAST.left[0] shouldBe DuplicateDeclaration("sum", LocationRange(LocationCoordinate(0, 1, 1), LocationCoordinate(32, 1, 33)), LocationRange(LocationCoordinate(34, 2, 1), LocationCoordinate(66, 2, 33)))
        }
    }
})


private fun parse(input: String): Either<List<Error>, Module> =
        za.co.no9.llvm.phases.parsing.parse(lexer(input))
                .mapLeft { listOf(it) }