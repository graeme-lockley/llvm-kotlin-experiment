package za.co.no9.llvm.parser

import io.kotlintest.shouldBe
import io.kotlintest.specs.BehaviorSpec
import za.co.no9.llvm.phases.parsing.*


class LexerTests : BehaviorSpec({
    fun symbols(text: String) =
            lexer(text) takeWhile { it !is EOF }


    Given("a blank string") {
        val text =
                ""

        When("a lexer is created") {
            val symbols =
                    lexer(text)

            Then("it has no symbols") {
                symbols.head is EOF
            }
        }
    }

    Given("\"123\"") {
        val text =
                "123"

        When("a lexer is created") {
            val symbols =
                    symbols(text)

            Then("it has a single symbol of [LiteralInt]") {
                symbols.size shouldBe 1

                symbols.all { it is LiteralInt } shouldBe true
            }

            Then("is has a list of lexemes [123]") {
                (symbols[0] as LiteralInt).value shouldBe 123
            }
        }
    }

    Given("\" 123 \"") {
        val text =
                " 123 "

        When("a lexer is created") {
            val symbols =
                    symbols(text)

            Then("it has a single symbol of [LiteralInt]") {
                symbols.size shouldBe 1

                symbols[0] as LiteralInt
            }

            Then("is has a single lexeme [123]") {
                (symbols[0] as LiteralInt).value shouldBe 123
            }
        }
    }

    Given("\"123 = hello ( ) +\"") {
        val text =
                "123 = hello ( ) +"

        When("a lexer is created") {
            val symbols =
                    symbols(text)

            Then("it has the symbols [LiteralInt, Operator, LowerID, LParen, RParen, Operator]") {
                symbols.size shouldBe 6

                symbols[0] as LiteralInt
                symbols[1] as Operator
                symbols[2] as LowerID
                symbols[3] as LParen
                symbols[4] as RParen
                symbols[5] as Operator
            }

            Then("is has the lexemes [123, \"=\", \"hello\", -, -, \"+\"]") {
                (symbols[0] as LiteralInt).value shouldBe 123
                (symbols[1] as Operator).lexeme shouldBe "="
                (symbols[2] as LowerID).lexeme shouldBe "hello"
                (symbols[5] as Operator).lexeme shouldBe "+"
            }
        }
    }

    Given("\"123 123.0 1.23e3\"") {
        val text =
                "123 123.0 1.23e3"

        When("a lexer is created") {
            val symbols =
                    symbols(text)

            Then("it has the symbols [LiteralInt, LiteralFloat, LiteralFloat]") {
                symbols.size shouldBe 3

                symbols[0] as LiteralInt
                symbols[1] as LiteralFloat
                symbols[2] as LiteralFloat
            }

            Then("is has the lexemes [123, 123.0, 1.23e3]") {
                (symbols[0] as LiteralInt).value shouldBe 123
                (symbols[1] as LiteralFloat).value shouldBe 123.0
                (symbols[2] as LiteralFloat).value shouldBe 1.23e3
            }
        }
    }

    Given("\"\"") {
        val text =
                "\"hello\" \"he\\\"llo\" \"\\uabab\uabac\""

        When("a lexer is created") {
            val symbols =
                    symbols(text)

            Then("it has the symbols [LiteralString, LiteralString, LiteralString]") {
                symbols.size shouldBe 3

                symbols[0] as LiteralString
                symbols[1] as LiteralString
                symbols[2] as LiteralString
            }

            Then("is has the lexemes [hello, he\"llo, \uABAB\uABAC]") {
                (symbols[0] as LiteralString).value shouldBe "hello"
                (symbols[1] as LiteralString).value shouldBe "he\"llo"
                (symbols[2] as LiteralString).value shouldBe "\uABAB\uABAC"
            }
        }
    }
})
