#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


extern int inc(int i);
extern int add(int a, int b);


#define LOOP_MAX 100000


void assertInc(int n) {
  if (inc(n) != n + 1) {
    printf("Error: failed for %d\n", n);
    exit(1);
  }
}


void testInc() {
  printf("forAll n -> inc(n) == n + 1\n");

  for (int i = 0; i < LOOP_MAX; i += 1) {
    assertInc(rand());
  }

  assertInc(INT_MAX);
  assertInc(INT_MIN);
}


void assertAdd(int a, int b) {
  if (add(a, b) != a + b) {
    printf("Error: failed for %d and %d\n", a, b);
    exit(1);
  }
}


void testAdd() {
  printf("forAll a, b -> add(a, b) == a + b\n");

  for (int i = 0; i < LOOP_MAX; i += 1) {
    assertAdd(rand(), rand());
  }

  assertAdd(INT_MAX, INT_MAX);
  assertAdd(INT_MIN, INT_MAX);
  assertAdd(INT_MIN, INT_MIN);
  assertAdd(INT_MAX, INT_MIN);
}


int main(int argc, char *argv[]) {
  srand(time(NULL));

  testInc();
  testAdd();

	return 0;
}

