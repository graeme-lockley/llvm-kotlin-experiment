package za.co.no9.llvm.data.parser

import io.kotlintest.shouldBe
import io.kotlintest.specs.ExpectSpec
import za.co.no9.llvm.data.sequence.Sequence
import za.co.no9.llvm.data.sequence.cons


private const val EOS =
        (-1).toChar()


class ParserTests : ExpectSpec({
    context("many") {
        expect("many(id)(\"abcd\") returns 4 ids of a, b, c and d respectively") {
            val satisfy =
                    many(id)(mkLexer("abcd"))

            satisfy as Ok

            satisfy.reply shouldBe listOf('a', 'b', 'c', 'd')
        }
    }


    context("sepBy1") {
        expect("sepBy1(id, comma)(\"a,b,c\")") {
            val satisfy =
                    sepBy1(id, comma)(mkLexer("a,b,c"))

            satisfy as Ok

            satisfy.reply shouldBe listOf('a', 'b', 'c')
        }


        expect("sepBy1(id, comma)(\"a\")") {
            val satisfy =
                    sepBy1(id, comma)(mkLexer("a"))

            satisfy as Ok

            satisfy.reply shouldBe listOf('a')
        }


        expect("sepBy1(id, comma)(\"\")") {
            val satisfy =
                    sepBy1(id, comma)(mkLexer(""))

            satisfy as Err

            satisfy.symbol shouldBe EOS
        }
    }


    context("sepBy") {
        expect("sepBy(id, comma)(\"a,b,c\")") {
            val satisfy =
                    sepBy(id, comma)(mkLexer("a,b,c"))

            satisfy as Ok

            satisfy.reply shouldBe listOf('a', 'b', 'c')
        }


        expect("sepBy(id, comma)(\"a\")") {
            val satisfy =
                    sepBy(id, comma)(mkLexer("a"))

            satisfy as Ok

            satisfy.reply shouldBe listOf('a')
        }


        expect("sepBy(id, comma)(\"\")") {
            val satisfy =
                    sepBy(id, comma)(mkLexer(""))

            satisfy as Ok

            satisfy.reply shouldBe listOf()
        }
    }

    context("between") {
        expect("between(lparen, rparen, many(id))(\"(ab)\"") {
            val satisfy =
                    between(lparen, rparen, many(id))(mkLexer("(ab)"))

            satisfy as Ok

            satisfy.reply shouldBe listOf('a', 'b')
        }
    }
})


val id =
        satisfy<Char> { it.isLetter() }

val lparen =
        char('(')

val rparen =
        char(')')

val comma =
        char(',')

fun char(c: Char) =
        satisfy<Char> { it == c }


fun mkLexer(input: String): Sequence<Char> =
        input.toSequence(EOS)


fun String.toSequence(oesChar: Char): Sequence<Char> =
        cons(if (this.isEmpty()) oesChar else this[0], { this.drop(1).toSequence(oesChar) })


//gcd (m: Int, n: Int): Int =
//  if (n == 0)
//    m
//  else
//    gcd (n, m `remainder` n)
//
//
//aValue: Int =
//  0


