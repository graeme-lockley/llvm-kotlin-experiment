package za.co.no9.llvm.data.lexer

import io.kotlintest.shouldBe
import io.kotlintest.specs.ExpectSpec
import za.co.no9.llvm.data.sequence.Sequence


private data class Symbol(val location: Location, val lexeme: String, val token: Int)


class LexerTest : ExpectSpec({
    context("lexer \"a\", \"b\", [0-9]+, \"//.*\", \"/*\"..\"*/\", \"(*\"..nested..\"*)\"") {
        val lexer =
                Builder(eofHandler = { l -> Symbol(l, "", 0) },
                        errorHandler = { l, s -> Symbol(l, s, -1) })
                        .add("a") { l, s -> Symbol(l, s, 1) }
                        .add("b") { l, s -> Symbol(l, s, 2) }
                        .add("[0-9]+") { l, s -> Symbol(l, s, 3) }
                        .add("//.*\\n") { l, s -> Symbol(l, s, 4) }
                        .add("/\\*", "\\*/", false) { l, s -> Symbol(l, s, 4) }
                        .add("\\(\\*", "\\*\\)", true) { l, s -> Symbol(l, s, 4) }
                        .build()

        fun Sequence<Symbol>.toList(): List<Symbol> =
                this.takeWhile { it.token != 0 }


        expect("\"\"") {
            lexer("").toList() shouldBe emptyList()
        }


        expect("\"c\"") {
            val input =
                    "c"

            lexer(input).toList().map { it.token } shouldBe listOf(-1)
            lexer(input).toList().map { it.lexeme } shouldBe listOf("c")
        }


        expect("\"123a45\"") {
            val input =
                    "123a45"

            lexer(input).toList().map { it.token } shouldBe listOf(3, 1, 3)
            lexer(input).toList().map { it.lexeme } shouldBe listOf("123", "a", "45")
        }


        expect("\"a// comment\\nb\"") {
            val input =
                    "a// comment\nb"

            lexer(input).toList().map { it.token } shouldBe listOf(1, 4, 2)
            lexer(input).toList().map { it.lexeme } shouldBe listOf("a", "// comment\n", "b")
        }


        expect("\"a/* comment\\nsome more */b\"") {
            val input =
                    "a/* comment\nsome more */b"

            lexer(input).toList().map { it.token } shouldBe listOf(1, 4, 2)
            lexer(input).toList().map { it.lexeme } shouldBe listOf("a", "/* comment\nsome more */", "b")
        }


        expect("\"a(* comment\\ns(*ome*) more *)b\"") {
            val input =
                    "a(* comment\ns(*ome*) more *)b"

            lexer(input).toList().map { it.token } shouldBe listOf(1, 4, 2)
            lexer(input).toList().map { it.lexeme } shouldBe listOf("a", "(* comment\ns(*ome*) more *)", "b")
        }
    }
})

