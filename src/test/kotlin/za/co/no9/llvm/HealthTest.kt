package za.co.no9.llvm

import arrow.core.Either
import arrow.core.Tuple3
import arrow.core.left
import arrow.core.right
import io.kotlintest.shouldBe
import io.kotlintest.specs.BehaviorSpec
import za.co.no9.llvm.data.either.Left
import za.co.no9.llvm.data.either.Right
import za.co.no9.llvm.phases.parsing.lexer
import za.co.no9.llvm.phases.parsing.parse
import za.co.no9.llvm.phases.typing.verify
import java.io.File
import java.io.IOException
import java.io.StringWriter
import java.util.concurrent.TimeUnit


class HealthTest : BehaviorSpec({
    Given("A configured build environment") {
        File("build/llvm").mkdirs()
        File("src/test/kotlin/za/co/no9/llvm/HealthRunner.c").copyTo(File("build/llvm/HealthRunner.c"), overwrite = true)

        And("the source file Health.sle") {
            val text =
                    File("src/test/kotlin/za/co/no9/llvm/Health.sle").readText()

            When("the input is successfully compiled") {
                val translation =
                        translate(text, "build/llvm/Health.ll")

                if (translation is Left)
                    println("Error: $translation")

                (translation is Right) shouldBe true

                And("the generated code is build") {
                    val runResult =
                            listOf(
                                    "llvm-as Health.ll -o Health.o",
                                    "clang -emit-llvm -c -S HealthRunner.c -o HealthRunner.ll",
                                    "llvm-as HealthRunner.ll -o HealthRunner.o",
                                    "llvm-link HealthRunner.o Health.o -o HealthRunner.bc",
                                    "lli HealthRunner.bc"
                            ).runCommands(File("build/llvm"))

                    runResult.map { "" } shouldBe "".right()

                    Then("the test run without an errors") {
                        val testResult =
                                listOf(
                                        "lli HealthRunner.bc"
                                ).runCommands(File("build/llvm"))

                        testResult.map { "" } shouldBe "".right()
                    }

                }
            }
        }
    }
})


fun translate(input: String, outputFileName: String) =
        parse(lexer(input))
                .mapLeft { listOf(it) }
                .andThen { verify(it) }
                .map { translateAST(it) }
                .map { File(outputFileName).writeText(it) }


fun translateAST(module: za.co.no9.llvm.ast.typedAST.Module): String {
    val ir =
            za.co.no9.llvm.phases.translating.translate(module)

    val result =
            StringWriter()

    za.co.no9.llvm.ir.translate(result, "Health.sle", ir)

    return result.toString()
}


fun List<String>.runCommands(workingDir: File): Either<Tuple3<String, Int, String>, String> {
    val initValue: Either<Tuple3<String, Int, String>, String> =
            "".right()

    return this.fold(initValue) { acc, cmd ->
        when (acc) {
            is Either.Left ->
                acc

            is Either.Right ->
                cmd.runCommand(workingDir)
        }
    }
}


fun String.runCommand(workingDir: File): Either<Tuple3<String, Int, String>, String> {
    return try {
        val parts =
                this.split("\\s".toRegex())

        val proc =
                ProcessBuilder(*parts.toTypedArray())
                        .directory(workingDir)
                        .redirectOutput(ProcessBuilder.Redirect.PIPE)
                        .redirectError(ProcessBuilder.Redirect.PIPE)
                        .start()

        proc.waitFor(60, TimeUnit.MINUTES)

        val exitValue =
                proc.exitValue()

        val text =
                proc.inputStream.bufferedReader().readText()

        if (exitValue == 0)
            text.right()
        else
            Tuple3(this, exitValue, text).left()
    } catch (e: IOException) {
        Tuple3(this, -1, e.message ?: "").left()
    }
}