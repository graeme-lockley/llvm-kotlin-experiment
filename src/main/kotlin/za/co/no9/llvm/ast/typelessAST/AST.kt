package za.co.no9.llvm.ast.typelessAST

import za.co.no9.llvm.data.lexer.Location
import za.co.no9.llvm.type.ReferencedType


sealed class Node(
        open val location: Location)


data class Module(
        val declarations: List<Declaration>)


sealed class Declaration(
        override val location: Location,
        open val name: String,
        open val expression: Expression) : Node(location)


data class FunctionDeclaration(
        override val location: Location,
        override val name: String,
        val arguments: List<Argument>,
        val returnReferencedType: ReferencedType,
        override val expression: Expression) : Declaration(location, name, expression)


data class ValueDeclaration(
        override val location: Location,
        override val name: String,
        val returnReferencedType: ReferencedType,
        override val expression: Expression) : Declaration(location, name, expression)


data class Argument(
        override val location: Location,
        val name: String,
        val referencedType: ReferencedType) : Node(location)

sealed class Expression(
        override val location: Location) : Node(location)


data class ConstantInt(
        override val location: Location,
        val value: Int) : Expression(location)

data class ConstantString(
        override val location: Location,
        val value: String) : Expression(location)

data class ConstantFloat(
        override val location: Location,
        val value: Float) : Expression(location)

data class ConstantBoolean(
        override val location: Location,
        val value: Boolean) : Expression(location)


data class ArgumentReference(
        override val location: Location,
        val name: String) : Expression(location)

data class BinaryOperation(
        override val location: Location,
        val left: Expression,
        val operator: String,
        val right: Expression) : Expression(location)

data class UnaryOperation(
        override val location: Location,
        val expression: Expression,
        val operator: String) : Expression(location)

data class NestedExpression(
        override val location: Location,
        val expression: Expression) : Expression(location)

data class IfExpression(
        override val location: Location,
        val ifExpression: Expression,
        val thenExpression: Expression,
        val elseExpression: Expression) : Expression(location)

data class CallExpression(
        override val location: Location,
        val name: String,
        val arguments: List<Expression>) : Expression(location)

