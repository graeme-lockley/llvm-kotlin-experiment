package za.co.no9.llvm.ast.typedAST

import za.co.no9.llvm.data.lexer.Location


sealed class Node(
        open val location: Location)


data class Module(
        val declarations: List<Declaration>)


data class Declaration(
        override val location: Location,
        val name: String,
        val arguments: List<String>,
        val expression: Expression) : Node(location)


sealed class Expression(
        override val location: Location) : Node(location)


data class ConstantInt(
        override val location: Location,
        val value: Int) : Expression(location)

data class ArgumentReference(
        override val location: Location,
        val name: String) : Expression(location)

data class Add(
        override val location: Location,
        val left: Expression,
        val right: Expression) : Expression(location)
