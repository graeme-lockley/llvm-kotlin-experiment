package za.co.no9.llvm.ir

import java.io.Writer


sealed class Type {
    abstract fun singleValue(): Boolean

    abstract fun asString(): String
}


object Void : Type() {
    override fun singleValue(): Boolean =
            false

    override fun asString(): String =
            "void"
}

data class I(val bits: Int) : Type() {
    override fun singleValue(): Boolean =
            true

    override fun asString(): String =
            "i$bits"
}

data class F(val bits: Int, val name: String) : Type() {
    override fun singleValue(): Boolean =
            true

    override fun asString(): String =
            name
}

val half = F(16, "half")
val float = F(32, "float")
val double = F(64, "double")
val fp128 = F(128, "fp128")
val x86_fp80 = F(80, "x86_fp80")
val ppc_mmx = F(128, "ppc_mmx")


data class Pointer(val type: Type) : Type() {
    override fun singleValue(): Boolean =
            true

    override fun asString(): String =
            type.asString() + "*"
}


sealed class Instruction {
    abstract fun write(writer: Writer)
}


data class Alloca(val result: String, val type: Type, val align: Int? = null) : Instruction() {
    override fun write(writer: Writer) {
        val alignSuffix =
                if (align == null)
                    ""
                else
                    ", align $align"

        writer.write("  $result = alloca ${type.asString()}$alignSuffix\n")
    }
}

data class Store(val type: Type, val value: String, val ptype: Type, val pointer: String, val align: Int? = null) : Instruction() {
    override fun write(writer: Writer) {
        val alignSuffix =
                if (align == null)
                    ""
                else
                    ", align $align"

        writer.write("  store ${type.asString()} $value, ${ptype.asString()} $pointer$alignSuffix\n")
    }
}

data class Load(val result: String, val type: Type, val ptype: Type, val pointer: String, val align: Int? = null) : Instruction() {
    override fun write(writer: Writer) {
        val alignSuffix =
                if (align == null)
                    ""
                else
                    ", align $align"

        writer.write("  $result = load ${type.asString()}, ${ptype.asString()} $pointer$alignSuffix\n")
    }
}

data class Ret(val type: Type, val value: String? = null) : Instruction() {
    override fun write(writer: Writer) {
        val valueSuffix =
                if (value == null)
                    ""
                else
                    " $value"

        writer.write("  ret ${type.asString()}$valueSuffix\n")
    }
}

data class Add(val result: String, val type: Type, val nuw: Boolean, val nsw: Boolean, val leftValue: String, val rightValue: String) : Instruction() {
    override fun write(writer: Writer) {
        val numValue =
                if (nuw) {
                    " nuw"
                } else {
                    ""
                }

        val nswValue =
                if (nsw) {
                    " nsw"
                } else {
                    ""
                }

        writer.write("  $result = add$numValue$nswValue ${type.asString()} $leftValue, $rightValue\n")
    }
}


data class Declare(val type: Type, val name: String, val arguments: List<Type>)

data class Define(val type: Type, val name: String, val arguments: List<Type>, val instructions: List<Instruction>)


data class Module(val declares: List<Declare>, val defines: List<Define>)


fun translate(writer: Writer, fileName: String, module: Module) {
    writer.write("; ModuleID = '$fileName'\n")
    writer.write("source_filename = \"$fileName\"\n")
    writer.write("target datalayout = \"e-m:o-i64:64-f80:128-n8:16:32:64-S128\"\n")
    writer.write("target triple = \"x86_64-apple-macosx10.14.0\"\n")
    writer.write("\n")

    module.declares.forEach { translate(writer, it) }
    module.defines.forEach { translate(writer, it) }

    if (module.defines.isNotEmpty()) {
        writer.write("attributes #0 = { noinline nounwind optnone ssp uwtable \"correctly-rounded-divide-sqrt-fp-math\"=\"false\" \"disable-tail-calls\"=\"false\" \"less-precise-fpmad\"=\"false\" \"min-legal-vector-width\"=\"0\" \"no-frame-pointer-elim\"=\"true\" \"no-frame-pointer-elim-non-leaf\" \"no-infs-fp-math\"=\"false\" \"no-jump-tables\"=\"false\" \"no-nans-fp-math\"=\"false\" \"no-signed-zeros-fp-math\"=\"false\" \"no-trapping-math\"=\"false\" \"stack-protector-buffer-size\"=\"8\" \"target-cpu\"=\"penryn\" \"target-features\"=\"+cx16,+fxsr,+mmx,+sahf,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87\" \"unsafe-fp-math\"=\"false\" \"use-soft-float\"=\"false\" }\n")
    }
    if (module.declares.isNotEmpty()) {
        writer.write("attributes #1 = { \"correctly-rounded-divide-sqrt-fp-math\"=\"false\" \"disable-tail-calls\"=\"false\" \"less-precise-fpmad\"=\"false\" \"no-frame-pointer-elim\"=\"true\" \"no-frame-pointer-elim-non-leaf\" \"no-infs-fp-math\"=\"false\" \"no-nans-fp-math\"=\"false\" \"no-signed-zeros-fp-math\"=\"false\" \"no-trapping-math\"=\"false\" \"stack-protector-buffer-size\"=\"8\" \"target-cpu\"=\"penryn\" \"target-features\"=\"+cx16,+fxsr,+mmx,+sahf,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87\" \"unsafe-fp-math\"=\"false\" \"use-soft-float\"=\"false\" }\n")
    }
    writer.write("\n")
    writer.write("!llvm.module.flags = !{!0, !1}\n")
    writer.write("!llvm.ident = !{!2}\n")
    writer.write("\n")
    writer.write("!0 = !{i32 1, !\"wchar_size\", i32 4}\n")
    writer.write("!1 = !{i32 7, !\"PIC Level\", i32 2}\n")
    writer.write("!2 = !{!\"clang version 8.0.0 (tags/RELEASE_800/final)\"}\n")
}


fun translate(writer: Writer, declare: Declare) {
    writer.write("declare ${declare.type.asString()} ${declare.name}(${declare.arguments.joinToString(", ") { it.asString() }}) #1\n")
    writer.write("\n")
}


fun translate(writer: Writer, define: Define) {
    writer.write("define ${define.type.asString()} ${define.name}(${define.arguments.joinToString(", ") { it.asString() }}) #0 {\n")
    define.instructions.forEach { it.write(writer) }
    writer.write("}\n")
    writer.write("\n")
}