package za.co.no9.llvm.type

import za.co.no9.llvm.data.lexer.Location


sealed class Type


object IntType : Type()

object StringType : Type()

object FloatType : Type()

object BooleanType : Type()


data class ReferencedType(val location: Location, val type: Type)