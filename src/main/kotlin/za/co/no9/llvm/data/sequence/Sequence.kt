package za.co.no9.llvm.data.sequence


interface Sequence<T> {
    val head: T

    val tail: Sequence<T>

    infix fun takeWhile(predicate: (T) -> Boolean): List<T> {
        val result =
                mutableListOf<T>()

        var current =
                this

        while (true) {
            val head =
                    current.head

            if (predicate(head)) {
                result.add(head)
                current = current.tail
            } else {
                return result
            }
        }
    }


    infix fun filter(predicate: (T) -> Boolean): Sequence<T> =
            if (predicate(head))
                cons(head, { tail filter predicate })
            else
                tail filter predicate


    infix fun <U> map(f: (T) -> U): Sequence<U> =
            cons(f(head), { tail map f })
}


fun <T> cons(car: T, cdr: () -> Sequence<T>): Sequence<T> =
        object : Sequence<T> {
            override val head =
                    car

            override val tail: Sequence<T>
                get() = cdr()
        }