package za.co.no9.llvm.data.parser

import za.co.no9.llvm.data.sequence.Sequence


typealias Parser<S, T> = (Sequence<S>) -> Reply<S, T>


sealed class Reply<S, T> {
    abstract fun <U> map(f: (T) -> U): Reply<S, U>
}

data class Ok<S, T>(val reply: T, val state: Sequence<S>) : Reply<S, T>() {
    override fun <U> map(f: (T) -> U) =
            Ok(f(reply), state)
}

data class Err<S, T>(val symbol: S, val alternatives: List<String>) : Reply<S, T>() {
    override fun <U> map(f: (T) -> U) =
            Err<S, U>(symbol, alternatives)
}


fun <S, T> of(t: T): Parser<S, T> =
        { state: Sequence<S> -> Ok(t, state) }


fun <S> satisfy(predicate: (S) -> Boolean): Parser<S, S> =
        { state ->
            if (predicate(state.head))
                Ok(state.head, state.tail)
            else
                Err(state.head, listOf())
        }


fun <S, X, Y> bind(p: Parser<S, X>, f: (X) -> Parser<S, Y>): Parser<S, Y> =
        { state ->
            when (val pResult = p(state)) {
                is Ok ->
                    f(pResult.reply)(pResult.state)

                is Err ->
                    Err(state.head, pResult.alternatives)
            }
        }


fun <S, X> or(ps: List<Parser<S, X>>): Parser<S, X> =
        { state ->
            var result: Reply<S, X>? =
                    null

            for (lp in 0 until ps.size) {
                if (result == null) {
                    val pResult =
                            ps[lp](state)

                    if (pResult is Ok)
                        result = pResult
                }
            }

            result ?: Err(state.head, listOf())
        }


fun <S, X, Y> Parser<S, X>.andThen(f: (X) -> Parser<S, Y>): Parser<S, Y> =
        bind(this, f)

fun <S, X, Y> Parser<S, X>.map(f: (X) -> Y): Parser<S, Y> =
        this.andThen { of<S, Y>(f(it)) }


fun <S, X> Parser<S, X>.mapErr(f: (Err<S, X>) -> Err<S, X>): Parser<S, X> =
        { state ->
            when (val reply = this(state)) {
                is Ok ->
                    reply

                is Err ->
                    f(reply)
            }
        }

fun <S, X> Parser<S, X>.mapErrAlternatives(f: (List<String>) -> List<String>): Parser<S, X> =
        this.mapErr { Err(it.symbol, f(it.alternatives)) }

fun <S, X> chainl1(p: Parser<S, X>, op: Parser<S, (X, X) -> X>): Parser<S, X> {
    fun rest(result: X): Parser<S, X> =
            or(listOf(
                    op.andThen { f -> p.andThen { rest(f(result, it)) } },
                    of(result)
            ))

    return p.andThen { rest(it) }
}

private fun <S, X> accumulateMany(p: Parser<S, X>): Parser<S, List<X>> =
        or(listOf(
                p.andThen { x -> accumulateMany(p).map { xs -> xs + x } },
                of(emptyList()))
        )

fun <S, X> many(p: Parser<S, X>): Parser<S, List<X>> =
        accumulateMany(p).map { it.reversed() }


fun <S, X, Y, Z> between(open: Parser<S, X>, close: Parser<S, Y>, between: Parser<S, Z>): Parser<S, Z> =
        open.andThen { between }.andThen { result -> close.map { result } }


fun <S, X, Y> sepBy1(p: Parser<S, X>, separator: Parser<S, Y>): Parser<S, List<X>> =
        p.andThen { x -> accumulateMany(separator.andThen { p }).map { xs -> (xs + x).reversed() } }

fun <S, X, Y> sepBy(p: Parser<S, X>, separator: Parser<S, Y>): Parser<S, List<X>> =
        or(listOf(
                p.andThen { x -> accumulateMany(separator.andThen { p }).map { xs -> (xs + x).reversed() } },
                of(emptyList())
        ))
