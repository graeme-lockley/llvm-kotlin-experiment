package za.co.no9.llvm.data.lexer

import za.co.no9.llvm.data.sequence.Sequence
import java.lang.Integer.max
import java.lang.Integer.min


sealed class Location {
    abstract operator fun plus(location: Location): Location
}


data class LocationCoordinate(val index: Int, val line: Int, val column: Int) : Location() {
    override operator fun plus(location: Location): Location =
            when (location) {
                is LocationCoordinate ->
                    if (location == this)
                        this
                    else
                        LocationRange(
                                LocationCoordinate(min(index, location.index), min(line, location.line), min(column, location.column)),
                                LocationCoordinate(max(index, location.index), max(line, location.line), max(column, location.column)))

                is LocationRange ->
                    LocationRange(
                            LocationCoordinate(min(index, location.start.index), min(line, location.start.line), min(column, location.start.column)),
                            LocationCoordinate(max(index, location.end.index), max(line, location.end.line), max(column, location.end.column)))
            }
}

data class LocationRange(val start: LocationCoordinate, val end: LocationCoordinate) : Location() {
    override operator fun plus(location: Location): Location =
            when (location) {
                is LocationCoordinate ->
                    location + this

                is LocationRange -> {
                    val startIndex =
                            min(start.index, location.start.index)

                    val endIndex =
                            max(end.index, location.end.index)

                    val startLocation =
                            LocationCoordinate(startIndex, min(start.line, location.start.line), min(start.column, location.start.column))

                    val endLocation =
                            LocationCoordinate(endIndex, max(end.line, location.end.line), max(end.column, location.end.column))

                    LocationRange(startLocation, endLocation)
                }
            }
}


typealias Handler<S> = (Location, String) -> S


private class MatchResult(val length: Int)


sealed class SymbolHandler<S> {
    abstract val handler: Handler<S>
}


data class SimpleSymbolHandler<S>(val pattern: Regex, override val handler: Handler<S>) : SymbolHandler<S>()

data class GatedSymbolHandler<S>(val openPattern: Regex, val closePattern: Regex, val nested: Boolean, override val handler: Handler<S>) : SymbolHandler<S>()


data class Builder<S>(
        private val eofHandler: (Location) -> S,
        private val errorHandler: Handler<S>,
        private val patterns: List<SymbolHandler<S>> = emptyList()) {
    fun add(pattern: String, handler: (Location, String) -> S): Builder<S> =
            Builder(eofHandler, errorHandler, patterns + listOf(SimpleSymbolHandler(pattern.toRegex(), handler)))

    fun add(openPattern: String, closePattern: String, nested: Boolean, handler: (Location, String) -> S): Builder<S> =
            Builder(eofHandler, errorHandler, patterns + listOf(GatedSymbolHandler(openPattern.toRegex(), closePattern.toRegex(), nested, handler)))

    fun build(): (String) -> Sequence<S> {
        val definition =
                Definition(eofHandler, errorHandler, patterns)

        return { input -> Lexer(definition, State(input, input.length, 0, 1, 1)) }
    }
}


private data class Skip(val state: State, val lexeme: String, val locationRange: Location)


private data class State(val input: String, val inputLength: Int, val index: Int, val line: Int, val column: Int) {
    fun skip(n: Int): Skip =
            if (index == inputLength) {
                Skip(this, "", LocationCoordinate(index, line, column))
            } else {
                var count =
                        0

                var runnerIndex =
                        index

                var runnerLine =
                        line

                var runnerColumn =
                        column

                fun bumpOne() {
                    if (input[runnerIndex] == '\n') {
                        runnerLine += 1
                        runnerColumn = 1
                    } else {
                        runnerColumn += 1
                    }
                    runnerIndex += 1
                    count += 1
                }

                val nLessOne =
                        if (n == 0) n else (n - 1)

                while (count < nLessOne && runnerIndex < inputLength) {
                    bumpOne()
                }

                val lexeme =
                        input.substring(index, runnerIndex + 1)

                val lexemeLocation =
                        if (index == runnerIndex && line == runnerLine && column == runnerColumn)
                            LocationCoordinate(index, line, column)
                        else
                            LocationRange(LocationCoordinate(index, line, column), LocationCoordinate(runnerIndex, runnerLine, runnerColumn))

                if (count < n && runnerIndex < inputLength) {
                    bumpOne()
                }

                Skip(State(input, inputLength, runnerIndex, runnerLine, runnerColumn), lexeme, lexemeLocation)
            }
}

private data class Definition<S>(
        val eofHandler: (Location) -> S,
        val errorHandler: Handler<S>,
        val patterns: List<SymbolHandler<S>>)


private data class Lexer<S>(val definition: Definition<S>, val state: State) : Sequence<S> {
    override val head: S

    private val nextState: State

    init {
        fun calculateCons(): Pair<S, State> {
            if (state.index >= state.inputLength) {
                return Pair(definition.eofHandler(LocationCoordinate(state.index, state.line, state.column)), state)
            } else {
                var lp = 0
                val lpLength =
                        definition.patterns.size

                while (true) {
                    if (lp == lpLength) {
                        val skip =
                                state.skip(1)

                        return Pair(definition.errorHandler(skip.locationRange, skip.lexeme), skip.state)
                    } else {
                        val symbolHandler =
                                definition.patterns[lp]

                        val matchResult =
                                match(symbolHandler, state)

                        if (matchResult == null) {
                            lp += 1
                        } else {
                            val skip =
                                    state.skip(matchResult.length)

                            return Pair(symbolHandler.handler(skip.locationRange, skip.lexeme), skip.state)
                        }
                    }
                }
            }
        }

        val cons =
                calculateCons()

        head = cons.first
        nextState = cons.second
    }

    override val tail: Sequence<S>
        get() = Lexer(definition, nextState)
}


private fun <S> match(symbolHandler: SymbolHandler<S>, state: State): MatchResult? =
        when (symbolHandler) {
            is SimpleSymbolHandler -> {
                val matchResult =
                        symbolHandler.pattern.find(state.input, state.index)

                if (matchResult == null || matchResult.range.first != state.index) {
                    null
                } else {
                    MatchResult(matchResult.value.length)
                }
            }

            is GatedSymbolHandler -> {
                val openMatchResult =
                        symbolHandler.openPattern.find(state.input, state.index)

                if (openMatchResult == null || openMatchResult.range.first != state.index) {
                    null
                } else if (symbolHandler.nested) {
                    var nextIndex =
                            openMatchResult.range.last + 1

                    var nesting =
                            1

                    var result: MatchResult? =
                            null

                    while (nesting > 0) {
                        val nextOpenMatchResult =
                                symbolHandler.openPattern.find(state.input, nextIndex)

                        val nextCloseMatchResult =
                                symbolHandler.closePattern.find(state.input, nextIndex)

                        if (nextCloseMatchResult == null) {
                            nesting = 0
                        } else if (nextOpenMatchResult != null && nextOpenMatchResult.range.first < nextCloseMatchResult.range.first) {
                            nesting += 1
                            nextIndex = nextOpenMatchResult.range.last + 1
                        } else {
                            nesting -= 1
                            nextIndex = nextCloseMatchResult.range.last + 1

                            if (nesting == 0) {
                                result = MatchResult(nextIndex - 1)
                            }
                        }
                    }

                    result
                } else {
                    val closeMatchResult =
                            symbolHandler.closePattern.find(state.input, state.index + openMatchResult.value.length)

                    if (closeMatchResult == null) {
                        null
                    } else {
                        MatchResult(closeMatchResult.range.last - state.index + 1)
                    }
                }
            }
        }
