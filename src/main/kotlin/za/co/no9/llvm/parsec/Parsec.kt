package za.co.no9.llvm.parsec


typealias Parser<T> = (State) -> ParserConsumed<T>


data class Position(val x: Int, val y: Int) {
    fun next(c: Char): Position =
            if (c == '\n') {
                Position(1, y + 1)
            } else {
                Position(x + 1, y)
            }
}


data class State(val input: String, val position: Position)

data class Message(val position: Position, val message: String, val alternatives: List<String>)

sealed class ParserConsumed<T>

data class Consumed<A>(val reply: Reply<A>) : ParserConsumed<A>()

data class Empty<A>(val reply: Reply<A>) : ParserConsumed<A>()


sealed class Reply<A>

data class Ok<A>(val reply: A, val state: State, val message: Message) : Reply<A>()

data class Error<A>(val message: Message) : Reply<A>()


fun <T> `return`(t: T): Parser<T> =
        { state: State -> Empty(Ok(t, state, Message(state.position, "", emptyList()))) }


fun satisfy(test: (Char) -> Boolean): Parser<Char> =
        { state: State ->
            val input =
                    state.input

            val pos =
                    state.position

            val c =
                    input.first()

            when (input) {
                "" ->
                    Empty(Error(Message(pos, "end of input", listOf())))

                else ->
                    if (test(c)) {
                        val newState =
                                State(input.drop(1), pos.next(c))

                        Consumed(Ok(c, newState, Message(pos, "", emptyList())))
                    } else
                        Empty(Error(Message(pos, input.first().toString(), listOf())))
            }
        }


fun <X, Y> bind(p: Parser<X>, f: (X) -> Parser<Y>): Parser<Y> =
        { input ->
            when (val parserResult = p(input)) {
                is Empty ->
                    when (val reply1 = parserResult.reply) {
                        is Ok ->
                            f(reply1.reply)(reply1.state)
                        is Error ->
                            Empty(Error(Message(reply1.message.position, reply1.message.message, reply1.message.alternatives)))
                    }

                is Consumed ->
                    when (val reply1 = parserResult.reply) {
                        is Ok ->
                            f(reply1.reply)(reply1.state)

                        is Error ->
                            Empty(Error(Message(reply1.message.position, reply1.message.message, reply1.message.alternatives)))
                    }
            }
        }


fun <X> or(p: Parser<X>, q: Parser<X>): Parser<X> =
        { input ->
            when (val pResult = p(input)) {
                is Empty ->
                    when (val pResultReply = pResult.reply) {
                        is Error -> {
                            val qResult =
                                    q(input)

                            when (qResult) {
                                is Empty ->
                                    when (val qResultReply = qResult.reply) {
                                        is Error ->
                                            mergeError(pResultReply.message, qResultReply.message)

                                        is Ok ->
                                            mergeOk(qResultReply.reply, qResultReply.state, pResultReply.message, qResultReply.message)
                                    }

                                is Consumed ->
                                    qResult

                            }
                        }

                        is Ok ->
                            when (val qResult = q(input)) {
                                is Empty ->
                                    when (val qResultReply = qResult.reply) {
                                        is Error ->
                                            mergeOk(pResultReply.reply, pResultReply.state, pResultReply.message, qResultReply.message)
                                        is Ok ->
                                            mergeOk(pResultReply.reply, pResultReply.state, pResultReply.message, qResultReply.message)
                                    }

                                is Consumed ->
                                    qResult
                            }
                    }

                is Consumed ->
                    pResult
            }
        }

private fun <T> mergeOk(x: T, state: State, msg1: Message, msg2: Message): ParserConsumed<T> =
        Empty(Ok(x, state, merge(msg1, msg2)))

private fun <T> mergeError(msg1: Message, msg2: Message): ParserConsumed<T> =
        Empty(Error(merge(msg1, msg2)))


private fun merge(msg1: Message, msg2: Message): Message =
        Message(msg1.position, msg1.message, msg1.alternatives + msg2.alternatives)


val char =
        { c: Char -> label(satisfy { it == c }, c.toString()) }

fun string(s: String): Parser<Unit> =
        when (s) {
            "" ->
                `return`(Unit)

            else ->
                bind(char(s.first()), { string(s.drop(1)) })
        }


fun <T> many1(p: Parser<T>): Parser<List<T>> =
        bind(p,
                { x ->
                    bind(or(many1(p), `return`(emptyList())),
                            { xs -> `return`(listOf(x) + xs) })
                })


fun <T> many(p: Parser<T>): Parser<List<T>> =
        or(p.andThen { x ->
            many(p).andThen { xs -> `return`(listOf(x) + xs) }
        }, `return`(emptyList()))

fun <T> label(p: Parser<T>, name: String): Parser<T> =
        { input ->
            when (val pState = p(input)) {
                is Empty ->
                    when (val pStateReply = pState.reply) {
                        is Error ->
                            Empty(Error(expect(pStateReply.message, name)))
                        is Ok ->
                            Empty(Ok(pStateReply.reply, pStateReply.state, expect(pStateReply.message, name)))
                    }
                else ->
                    pState
            }
        }

fun expect(msg: Message, name: String): Message =
        Message(msg.position, msg.message, listOf(name))


//val letter =
//        label(satisfy { it.isLetter() }, "letter")
//
//val digit =
//        label(satisfy { it.isDigit() }, "digit")
//
//
//val fidentifier =
//        or(letter, digit).andThen { l ->
//            many(letter).andThen { ls -> `return`(listOf(l) + ls) }
//        }
//
//val identifier =
//        letter.andThen { l ->
//            many(letter).andThen { ls -> `return`(listOf(l) + ls) }
//        }


fun <X, Y> Parser<X>.andThen(f: (X) -> Parser<Y>): Parser<Y> =
        bind(this, f)