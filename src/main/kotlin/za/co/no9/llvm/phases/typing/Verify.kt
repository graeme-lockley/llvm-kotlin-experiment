package za.co.no9.llvm.phases.typing

import za.co.no9.llvm.ast.typelessAST.*
import za.co.no9.llvm.data.either.Either
import za.co.no9.llvm.data.either.Left
import za.co.no9.llvm.data.either.Right
import za.co.no9.llvm.data.lexer.Location
import za.co.no9.llvm.phases.DuplicateDeclaration
import za.co.no9.llvm.phases.Error
import za.co.no9.llvm.phases.UnknownNameReference


fun verify(module: Module): Either<List<Error>, za.co.no9.llvm.ast.typedAST.Module> {
    val errors =
            verifyArgumentReferences(module) +
                    verifyDuplicateDeclarationNames(module)

    return if (errors.isEmpty())
        Right(module.toTypedAST())
    else
        Left(errors)
}


private fun Module.toTypedAST(): za.co.no9.llvm.ast.typedAST.Module =
        za.co.no9.llvm.ast.typedAST.Module(this.declarations.map { it.toTypedAST() })


private fun Declaration.toTypedAST(): za.co.no9.llvm.ast.typedAST.Declaration =
        when (this) {
            is FunctionDeclaration ->
                za.co.no9.llvm.ast.typedAST.Declaration(this.location, this.name, this.arguments.map { it.name }, this.expression.toTypedAST())

            else ->
                TODO("Unsupported ${this}")
        }


private fun Expression.toTypedAST(): za.co.no9.llvm.ast.typedAST.Expression =
        when (this) {
            is ConstantInt ->
                za.co.no9.llvm.ast.typedAST.ConstantInt(this.location, this.value)

            is ArgumentReference ->
                za.co.no9.llvm.ast.typedAST.ArgumentReference(this.location, this.name)

            is BinaryOperation ->
                when (this.operator) {
                    "+" ->
                        za.co.no9.llvm.ast.typedAST.Add(this.location, this.left.toTypedAST(), this.right.toTypedAST())

                    else ->
                        TODO("Unsupported operator: ${this.operator}")
                }

            else ->
                TODO("Unsupported expression translation: $this")
        }


private fun verifyDuplicateDeclarationNames(module: Module): List<Error> =
        module.declarations.fold(Pair(emptyMap<String, Location>(), emptyList<Error>()), { acc, declaration ->
            if (acc.first.containsKey(declaration.name))
                acc mapSecond { it + DuplicateDeclaration(declaration.name, acc.first.getValue(declaration.name), declaration.location) }
            else
                acc mapFirst { it + Pair(declaration.name, declaration.location) }
        }).second


private fun verifyArgumentReferences(module: Module): List<Error> =
        module.declarations.flatMap { verifyArgumentReferences(it) }


private fun verifyArgumentReferences(declaration: Declaration): List<Error> {
    if (declaration is FunctionDeclaration) {
        val arguments =
                declaration.arguments.toSet()

        fun verify(expression: Expression): List<Error> =
                when (expression) {
                    is ArgumentReference ->
                        if (arguments.map { it.name }.contains(expression.name))
                            emptyList()
                        else
                            listOf(UnknownNameReference(expression.name, expression.location))

                    is BinaryOperation ->
                        verify(expression.left) + verify(expression.right)

                    else ->
                        emptyList()
                }

        return verify(declaration.expression)
    } else {
        return emptyList()
    }
}


private infix fun <F, S, N> Pair<F, S>.mapFirst(f: (F) -> N): Pair<N, S> =
        Pair(f(this.first), second)

private infix fun <F, S, N> Pair<F, S>.mapSecond(f: (S) -> N): Pair<F, N> =
        Pair(first, f(second))
