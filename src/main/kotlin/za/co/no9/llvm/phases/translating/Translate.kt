package za.co.no9.llvm.phases.translating

import za.co.no9.llvm.ast.typedAST.*
import za.co.no9.llvm.ast.typedAST.Add
import za.co.no9.llvm.ast.typedAST.Module
import za.co.no9.llvm.ir.*


private val i32 =
        I(32)

private val i32p =
        Pointer(i32)


fun translate(ast: Module): za.co.no9.llvm.ir.Module =
        za.co.no9.llvm.ir.Module(emptyList(), ast.declarations.map { translate(it) })


private fun translate(ast: Declaration): Define =
        TranslateDeclaration().translate(ast)


private class TranslateDeclaration {
    var variableCount =
            0

    var instructions =
            mutableListOf<Instruction>()

    var environment =
            mutableMapOf<String, String>()


    fun translate(ast: Declaration): Define {
        variableCount = ast.arguments.size + 1
        instructions = mutableListOf()
        environment = mutableMapOf()

        ast.arguments.forEach {
            val reference =
                    newVariableReference()

            environment[it] = reference

            addInstruction(Alloca(reference, i32, 4))
        }

        ast.arguments.forEachIndexed { index, name ->
            addInstruction(Store(i32, "%$index", i32p, environment[name]!!, 4))
        }

        val resultReference =
                translate(ast.expression)

        addInstruction(Ret(i32, resultReference))

        return Define(i32, "@${ast.name}", ast.arguments.map { i32 }, instructions)
    }


    private fun translate(ast: Expression): String =
            when (ast) {
                is ConstantInt ->
                    ast.value.toString()

                is ArgumentReference -> {
                    val variableReference =
                        environment[ast.name]!!

                    val newReference =
                            newVariableReference()

                    addInstruction(Load(newReference, i32, i32p, variableReference, 4))

                    newReference
                }

                is Add -> {
                    val leftReference =
                            translate(ast.left)

                    val rightReference =
                            translate(ast.right)

                    val newReference =
                            newVariableReference()

                    addInstruction(za.co.no9.llvm.ir.Add(newReference, i32, false, true, leftReference, rightReference))

                    newReference
                }
            }


    private fun addInstruction(instruction: Instruction) {
        instructions.add(instruction)
    }

    private fun newVariableReference(): String {
        val result =
                "%$variableCount"

        variableCount += 1

        return result
    }
}