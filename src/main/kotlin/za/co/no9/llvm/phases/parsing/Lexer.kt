package za.co.no9.llvm.phases.parsing

import org.apache.commons.text.StringEscapeUtils.unescapeJava
import za.co.no9.llvm.data.lexer.Builder
import za.co.no9.llvm.data.lexer.Location


sealed class Symbol(
        open val location: Location)


data class EOF(
        override val location: Location) : Symbol(location)

data class ERROR(
        override val location: Location,
        val lexeme: String) : Symbol(location) {

    override fun toString(): String =
            lexeme
}

data class WhiteSpace(
        override val location: Location,
        val lexeme: String) : Symbol(location)

data class Operator(
        override val location: Location,
        val lexeme: String) : Symbol(location)

data class LowerID(
        override val location: Location,
        val lexeme: String) : Symbol(location)

data class UpperID(
        override val location: Location,
        val lexeme: String) : Symbol(location)

data class LiteralInt(
        override val location: Location,
        val value: Int) : Symbol(location)

data class LiteralFloat(
        override val location: Location,
        val value: Float) : Symbol(location)

data class LiteralString(
        override val location: Location,
        val value: String) : Symbol(location)

data class LParen(
        override val location: Location) : Symbol(location)

data class RParen(
        override val location: Location) : Symbol(location)


private val lexerBuilder =
        Builder(
                eofHandler = { EOF(it) },
                errorHandler = { l, s -> ERROR(l, s) })

                .add("\\s+") { l, s -> WhiteSpace(l, s) }
                .add("//.*\\n") { l, s -> WhiteSpace(l, s) }
                .add("/\\*", "\\*/", true) { l, s -> WhiteSpace(l, s) }

                .add("(\\d+\\.\\d+|\\.\\d+|\\d+\\.)([eE]\\d+)?") { l, s -> LiteralFloat(l, s.toFloat()) }
                .add("[0-9]+") { l, s -> LiteralInt(l, s.toInt()) }
                .add("\"([^\"\\r\\n\\\\]|\\\\[btnfr\"'\\\\]|\\\\u[0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F])*\"") { l, s -> LiteralString(l, unescapeJava(s.drop(1).dropLast(1))) }
                .add("[a-z][a-zA-Z0-9_]*'*") { l, s -> LowerID(l, s) }
                .add("[A-Z][a-zA-Z0-9_]*'*") { l, s -> UpperID(l, s) }

                .add("\\(") { l, _ -> LParen(l) }
                .add("\\)") { l, _ -> RParen(l) }
                .add("[-=,+*/<>!&|:%]+") { l, s -> Operator(l, s) }

                .build()


fun lexer(text: String) =
        lexerBuilder(text) filter { it !is WhiteSpace }


