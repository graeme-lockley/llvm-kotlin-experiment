package za.co.no9.llvm.phases

import za.co.no9.llvm.data.lexer.Location
import za.co.no9.llvm.phases.parsing.Symbol

sealed class Error


data class ExpectedTokensError(
        val found: Symbol,
        val tokens: List<String>) : Error()


data class UnknownNameReference(
        val name: String,
        val location: Location) : Error()

data class DuplicateDeclaration(
        val name: String,
        val first: Location,
        val duplicate: Location): Error()