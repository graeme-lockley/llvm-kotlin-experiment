package za.co.no9.llvm.phases.parsing

import za.co.no9.llvm.ast.typelessAST.*
import za.co.no9.llvm.data.either.Either
import za.co.no9.llvm.data.either.Left
import za.co.no9.llvm.data.either.Right
import za.co.no9.llvm.data.parser.*
import za.co.no9.llvm.data.sequence.Sequence
import za.co.no9.llvm.phases.Error
import za.co.no9.llvm.phases.ExpectedTokensError
import za.co.no9.llvm.type.*


private val symbol =
        { p: (Symbol) -> Boolean -> satisfy(p) }

private val operator: (String) -> Parser<Symbol, Symbol> =
        { text -> symbol { it is Operator && it.lexeme == text } }


private val ampersandAmpersand =
        operator("&&")

private val bang =
        operator("!")

private val bangEqual =
        operator("!=")

private val barBar =
        operator("||")

private val colon =
        operator(":")

private val comma =
        operator(",")

private val equal =
        operator("=")

private val equalEqual =
        operator("==")

private val greater =
        operator(">")

private val greaterEqual =
        operator(">=")

private val less =
        operator("<")

private val lessEqual =
        operator("<=")

private val lparen =
        symbol { it is LParen }

private val minus =
        operator("-")

private val percentage =
        operator("%")

private val plus =
        operator("+")

private val rparen =
        symbol { it is RParen }

private val slash =
        operator("/")

private val star =
        operator("*")


private fun parseExpression(): Parser<Symbol, Expression> =
        expression


val factor: Parser<Symbol, Expression> =
        or(listOf(
                symbol { it is LowerID }.map { ArgumentReference(it.location, (it as LowerID).lexeme) as Expression },
                symbol { it is LiteralInt }.map { ConstantInt(it.location, (it as LiteralInt).value) as Expression },
                symbol { it is LiteralFloat }.map { ConstantFloat(it.location, (it as LiteralFloat).value) as Expression },
                symbol { it is LiteralString }.map { ConstantString(it.location, (it as LiteralString).value) as Expression },
                symbol { it is UpperID && it.lexeme == "True" }.map { ConstantBoolean(it.location, true) as Expression },
                symbol { it is UpperID && it.lexeme == "False" }.map { ConstantBoolean(it.location, false) as Expression },
                lparen.andThen { left -> parseExpression().andThen { expression -> rparen.map { right -> NestedExpression(left.location + right.location, expression) as Expression } } }
        )).mapErrAlternatives { listOf("LowerID", "LiteralInt") }


val unaryExpressionNotPlusMinus: Parser<Symbol, Expression> =
        or(listOf(
                bang.andThen { p -> factor.map { UnaryOperation(p.location + it.location, it, "!") as Expression } },
                factor
        ))


val unaryExpression: Parser<Symbol, Expression> =
        or(listOf(
                plus.andThen { p -> unaryExpressionNotPlusMinus.map { UnaryOperation(p.location + it.location, it, "+") as Expression } },
                minus.andThen { p -> unaryExpressionNotPlusMinus.map { UnaryOperation(p.location + it.location, it, "-") as Expression } },
                unaryExpressionNotPlusMinus
        ))


private val mulOp: Parser<Symbol, (Expression, Expression) -> Expression> =
        or(listOf(
                star.map { { e1: Expression, e2: Expression -> BinaryOperation(e1.location + e2.location, e1, "*", e2) as Expression } },
                slash.map { { e1: Expression, e2: Expression -> BinaryOperation(e1.location + e2.location, e1, "/", e2) as Expression } },
                percentage.map { { e1: Expression, e2: Expression -> BinaryOperation(e1.location + e2.location, e1, "%", e2) as Expression } }
        ))

val multiplicativeExpression: Parser<Symbol, Expression> =
        chainl1(factor, mulOp)


private val addOp: Parser<Symbol, (Expression, Expression) -> Expression> =
        or(listOf(
                plus.map { { e1: Expression, e2: Expression -> BinaryOperation(e1.location + e2.location, e1, "+", e2) as Expression } },
                minus.map { { e1: Expression, e2: Expression -> BinaryOperation(e1.location + e2.location, e1, "-", e2) as Expression } }
        ))

val additiveExpression: Parser<Symbol, Expression> =
        chainl1(factor, addOp)


private val relationalOp: Parser<Symbol, (Expression, Expression) -> Expression> =
        or(listOf(
                less.map { { e1: Expression, e2: Expression -> BinaryOperation(e1.location + e2.location, e1, "<", e2) as Expression } },
                lessEqual.map { { e1: Expression, e2: Expression -> BinaryOperation(e1.location + e2.location, e1, "<=", e2) as Expression } },
                greater.map { { e1: Expression, e2: Expression -> BinaryOperation(e1.location + e2.location, e1, ">", e2) as Expression } },
                greaterEqual.map { { e1: Expression, e2: Expression -> BinaryOperation(e1.location + e2.location, e1, ">=", e2) as Expression } }
        ))

val relationalExpression: Parser<Symbol, Expression> =
        chainl1(additiveExpression, relationalOp)


private val equalityOp: Parser<Symbol, (Expression, Expression) -> Expression> =
        or(listOf(
                equalEqual.map { { e1: Expression, e2: Expression -> BinaryOperation(e1.location + e2.location, e1, "==", e2) as Expression } },
                bangEqual.map { { e1: Expression, e2: Expression -> BinaryOperation(e1.location + e2.location, e1, "!=", e2) as Expression } }
        ))

val equalityExpression: Parser<Symbol, Expression> =
        chainl1(relationalExpression, equalityOp)


private val conditionalAndOp: Parser<Symbol, (Expression, Expression) -> Expression> =
        ampersandAmpersand.map { { e1: Expression, e2: Expression -> BinaryOperation(e1.location + e2.location, e1, "&&", e2) as Expression } }

val conditionalAndExpression: Parser<Symbol, Expression> =
        chainl1(equalityExpression, conditionalAndOp)


private val conditionalOrOp: Parser<Symbol, (Expression, Expression) -> Expression> =
        barBar.map { { e1: Expression, e2: Expression -> BinaryOperation(e1.location + e2.location, e1, "||", e2) as Expression } }

val conditionalOrExpression: Parser<Symbol, Expression> =
        chainl1(equalityExpression, conditionalOrOp)


val expression: Parser<Symbol, Expression> =
        conditionalOrExpression


val type: Parser<Symbol, ReferencedType> =
        or(listOf(
                symbol { it is UpperID && it.lexeme == "Boolean" }.map { ReferencedType(it.location, BooleanType) },
                symbol { it is UpperID && it.lexeme == "String" }.map { ReferencedType(it.location, StringType) },
                symbol { it is UpperID && it.lexeme == "Int" }.map { ReferencedType(it.location, IntType) },
                symbol { it is UpperID && it.lexeme == "Float" }.map { ReferencedType(it.location, FloatType) }
        )).mapErrAlternatives { listOf("Boolean", "String", "Int", "Float") }


val argument: Parser<Symbol, Argument> =
        symbol { it is LowerID }.andThen { name ->
            colon.andThen { type.map { Argument(name.location + it.location, (name as LowerID).lexeme, it) } }
        }


val declaration: Parser<Symbol, Declaration> =
        symbol { it is LowerID }.andThen { name ->
            or(listOf(
                    between(lparen, rparen, sepBy(argument, comma)).andThen { arguments ->
                        colon.andThen {
                            type.andThen { type ->
                                equal.andThen {
                                    expression.map {
                                        FunctionDeclaration((name as LowerID).location + it.location, name.lexeme, arguments, type, it) as Declaration
                                    }
                                }
                            }
                        }
                    },
                    colon.andThen {
                        type.andThen { type ->
                            equal.andThen {
                                expression.map {
                                    ValueDeclaration((name as LowerID).location + it.location, name.lexeme, type, it) as Declaration
                                }
                            }
                        }
                    }
            ))
        }


val module: Parser<Symbol, Module> =
        many(declaration).andThen { declarations -> symbol { it is EOF }.map { Module(declarations) } }


fun parse(lexer: Sequence<Symbol>): Either<Error, Module> =
        when (val result = module(lexer)) {
            is Ok ->
                Right(result.reply)

            is Err ->
                Left(ExpectedTokensError(result.symbol, result.alternatives))
        }